package org.kalkuta;

// Класс Грузовой автомобиль
class Truck extends Vehicle implements Runnable{

    private final int size = 7;
    private final int capacity = 8;
    private final int unloadingTime = 12;

    Truck(Hangar hangar, Store store, int num) {
        super(hangar,store,num);
    }

    public void run() {
        while(true){
            hangar.getAccess(size);
            //if(isAccess){
                System.out.println("Truck-" + getNum() + " в ангаре " );
                for (int i = 1; i <= capacity; i++) {
                    store.get();
                    System.out.println("Truck-" + getNum() + " загрузил " + i + " товар");
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                hangar.left(size);
                System.out.println("Truck-" + getNum() + " уехал на отгрузку");
                try {
                    //Отгрузка
                    Thread.sleep(unloadingTime*1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
    }
    public int getSize() {
        return size;
    }

    public int getCapacity() {
        return capacity;
    }

    public int getUnloadingTime() {
        return unloadingTime;
    }
}
