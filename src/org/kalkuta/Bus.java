package org.kalkuta;

// Класс Автобус
class Bus extends Vehicle implements Runnable{
    private final int size = 5;
    private final int capacity = 4;
    private final int unloadingTime = 8;

    Bus(Hangar hangar, Store store, int num) {
        super(hangar,store,num);
    }

    public void run() {
        while(true){
            boolean isAccess = hangar.getAccess(size);
            if(isAccess){
                System.out.println("Bus-" + getNum() + " в ангаре ");
                for (int i = 1; i <= capacity; i++) {
                    store.get();
                    System.out.println("Bus-" + getNum() + " загрузил " + i + " товар");
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                hangar.left(size);
                System.out.println("Bus-" + getNum() + " уехал на отгрузку");
                try {
                    //Отгрузка
                    Thread.sleep(unloadingTime*1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public int getSize() {
        return size;
    }

    public int getCapacity() {
        return capacity;
    }

    public int getUnloadingTime() {
        return unloadingTime;
    }
}
