package org.kalkuta;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

// Класс Склад, хранящий коробки
class Store {
    private int box = 0;
    ReentrantLock locker;
    Condition condition;

    Store(){
        locker = new ReentrantLock(); // создаем блокировку
        condition = locker.newCondition(); // получаем условие, связанное с блокировкой
    }

    public void get() {

        locker.lock();
        try{
            // пока нет доступных коробок на складе, ожидаем
            while (box < 1)
                condition.await();

            box--;
            System.out.println("Коробок на складе: " + box);

            // сигнализируем
            condition.signalAll();
        }
        catch (InterruptedException e){
            System.out.println(e.getMessage());
        }
        finally{
            locker.unlock();
        }
    }
    public void put() {

        locker.lock();
        try{
            // пока на складе до 30 коробок, ждем освобождения места
            while (box >=25)
                condition.await();

            box = box+5;    //Коробки производятся со скоростью 5 коробок каждые 2 секунды.
            System.out.println("Производитель добавил 5 коробок на склад");
            System.out.println("Коробок на складе после добавления: " + box);
            // сигнализируем
            condition.signalAll();
        }
        catch (InterruptedException e){
            System.out.println(e.getMessage());
        }
        finally{
            locker.unlock();
        }
    }
}
