package org.kalkuta;

// класс Производитель Коробок
class Producer implements Runnable{

    private Store store;
    Producer(Store store){
        this.store=store;
    }
    public void run(){
        while(true) {
            store.put();
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
