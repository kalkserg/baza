package org.kalkuta;

// Класс Легковой автомобиль
class Car extends Vehicle implements Runnable {

    private final int size = 4;
    private final int capacity = 2;
    private final int unloadingTime = 4;

    Car(Hangar hangar, Store store, int num) {
        super(hangar,store,num);
    }

    public void run() {
        while(true){
            boolean isAccess = hangar.getAccess(size);
            if(isAccess){
                System.out.println("Car-" + getNum() + " в ангаре ");
                for (int i = 1; i <= capacity; i++) {
                    store.get();
                    System.out.println("Car-" + getNum() + " загрузил " + i + " товар");
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                hangar.left(size);
                System.out.println("Car-" + getNum() + " уехал на отгрузку");
                try {
                    //Отгрузка
                    Thread.sleep(unloadingTime*1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public int getSize() {
        return size;
    }

    public int getCapacity() {
        return capacity;
    }

    public int getUnloadingTime() {
        return unloadingTime;
    }
}
