package org.kalkuta;

public class Program {

    public static void main(String[] args) {

        Store store=new Store();
        Hangar hangar =new Hangar(15);
        Producer producer = new Producer(store);

        Car car1 = new Car(hangar, store,1);
        Car car2 = new Car(hangar, store,2);
        Car car3 = new Car(hangar, store,3);
        Car car4 = new Car(hangar, store,4);

        Bus bus1 = new Bus(hangar, store,1);
        Bus bus2 = new Bus(hangar, store,2);
        Bus bus3 = new Bus(hangar, store,3);
        Bus bus4 = new Bus(hangar, store,4);

        Truck truck1 = new Truck(hangar, store,1);
        Truck truck2 = new Truck(hangar, store,2);
        Truck truck3 = new Truck(hangar, store,3);
        Truck truck4 = new Truck(hangar, store,4);


        new Thread(producer).start();
//        new Thread(car1).start();
//        new Thread(car2).start();
//        new Thread(car3).start();
//        new Thread(car4).start();

        new Thread(bus1).start();
        new Thread(bus2).start();
        new Thread(bus3).start();
        new Thread(bus4).start();

//        new Thread(truck1).start();
//        new Thread(truck2).start();
//        new Thread(truck3).start();
//        new Thread(truck4).start();
    }
}

