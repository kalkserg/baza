package org.kalkuta;

// Абстрактный Класс автомобиль
abstract class Vehicle {

    private int num;
    Store store;
    Hangar hangar;

    Vehicle(Hangar hangar, Store store, int num) {
        this.hangar = hangar;
        this.store = store;
        this.num = num;
    }

    public int getNum() {
        return num;
    }
}
