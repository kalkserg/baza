package org.kalkuta;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

// Класс Ангар
public class Hangar {
    private Integer hangarSize;
    //private Store store;
    ReentrantLock locker;
    Condition condition;

    Hangar(Integer size){
        locker = new ReentrantLock(); // создаем блокировку
        condition = locker.newCondition(); // получаем условие, связанное с блокировкой
        hangarSize= size;
    }

    public boolean getAccess(int carSize) {
        boolean isAccess = false;

        locker.lock();

        try{
            // пока нет места в ангаре, ожидаем
            while (hangarSize < 1){
                condition.await();
                isAccess = false;
            }
            if(hangarSize >= carSize) {
                hangarSize = hangarSize-carSize;
                System.out.println("Место в ангаре уменьшилось: " + hangarSize);

                // сигнализируем
                condition.signalAll();
                isAccess = true;
            }
        }
        catch (InterruptedException e){
            System.out.println(e.getMessage());
        }
        finally{
            locker.unlock();
        }
        return isAccess;
    }

    public void left(int carSize) {

        locker.lock();

        hangarSize = hangarSize+carSize;
        System.out.println("Место в ангаре освободилось: " + hangarSize);

        // сигнализируем
        condition.signalAll();

        locker.unlock();
    }
}
